import React from "react";
import CenterLayout from "../Components/Layout/CenterLayout";
import { Input, SectionContainer, ImageRatio } from "../Components";
import Link from "next/link";

const signin = (): JSX.Element => {
	return (
		<CenterLayout headerPosition="initial" pageTitle="Games Hubs | Sineup">
			<SectionContainer classList="container mx-auto px-4 relative">
				<form className="w-full sm:w-448 mx-auto rounded-8 rounded-t-none border-3 border-brand-1 px-8 py-24 text-center relative">
					<div
						className="absolute right-1/2 transform translate-x-1/2 w-64"
						style={{ top: "-50px" }}
					>
						<h2 className="text-5xl">خوش آمدید</h2>
					</div>
					<Input
						placeholder="نام کاربری"
						value=""
						type="text"
						inputClass="block w-full rounded-none mt-4 mb-3"
					/>
					<Input
						placeholder="پسوورد"
						value=""
						type="text"
						inputClass="block w-full rounded-none  mb-3"
					/>
					<button className="btn mt-12 w-64 py-3" type="submit">
						ورود
					</button>
					<div className="flex justify-evenly">
						<div className="text-sm mt-2">
							<Link href="/signup">
								<a className="px-2">ثبت نام</a>
							</Link>
						</div>
						<div className="text-sm mt-2">
							<Link href="/signup">
								<a className="px-2">فراموشی رمز ورود</a>
							</Link>
						</div>
					</div>
				</form>
			</SectionContainer>
		</CenterLayout>
	);
};

export default signin;
