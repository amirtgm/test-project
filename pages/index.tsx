import Layout from "../Components/Layout/Layout";
import storeWrapper, { RootState } from "../store";
import { connect, useSelector } from "react-redux";
import { fetchHome } from "../store/actions";
import {
	Showcase,
	SectionContainer,
	Categories,
	NewsSection,
	TrendsList,
	TrendSlider,
} from "../Components";

const Home = (): JSX.Element => {
	const homeState = useSelector<RootState, HomeState>((state) => state.home);
	return (
		<Layout pageTitle="Games Hub">
			<SectionContainer classList="mb-8">
				<Showcase />
			</SectionContainer>
			<SectionContainer
				classList="container mx-auto px-4 relative mb-10"
				sectionTitle="داغ‌ترین‌ها"
			>
				<>
					<TrendsList />
					<TrendSlider trendList={homeState.home.games} />
				</>
			</SectionContainer>
			<SectionContainer
				classList="container mx-auto px-4 mb-10 mb-8"
				sectionTitle="دسته بندی ها"
			>
				<Categories categoriesList={homeState.home.categories} />
			</SectionContainer>
			<SectionContainer
				sectionTitle="اخبار"
				classList="container mx-auto px-4 mb-10"
			>
				<NewsSection newsList={homeState.home.posts} />
			</SectionContainer>
		</Layout>
	);
};

export const getServerSideProps = storeWrapper.getServerSideProps(
	async ({ store, req, res, query, ...etc }) => {
		await store.dispatch<any>(fetchHome());
	}
);
export default connect()(Home);
