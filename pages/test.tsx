import Layout from "../Components/Layout/Layout";
import { connect, useSelector } from "react-redux";
import { SectionContainer } from "../Components";
import ReduxTest from "../Components/ReduxTest/ReduxTest";
import storeWrapper from "../store";
import { fetchPosts } from "../store/actions/posts.action";

// interface IIndex {}
const Test = (): JSX.Element => {
	const store = useSelector((state) => state);
	return (
		<Layout pageTitle={`Games Hubs`}>
			<SectionContainer classList="container mx-auto px-4 relative mb-10">
				<ReduxTest />
			</SectionContainer>
		</Layout>
	);
};
export const getServerSideProps = storeWrapper.getServerSideProps(
	async ({ store, req, res, ...etc }) => {
		await store.dispatch<any>(fetchPosts());
	}
);

export default connect((state) => state)(Test);
