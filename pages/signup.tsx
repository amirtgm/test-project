import React from "react";
import Image from "next/image";
import CenterLayout from "../Components/Layout/CenterLayout";
import { Input, SectionContainer, ImageRatio } from "../Components";
import Link from "next/link";

const Signup = (): JSX.Element => {
	return (
		<CenterLayout headerPosition="initial" pageTitle="Games Hubs | Sineup">
			<SectionContainer classList="container mx-auto px-4 relative">
				<form className="w-full sm:w-448 mx-auto rounded-8 rounded-t-none border-3 border-brand-1 px-8 py-24 text-center relative">
					<div
						className="w-48 absolute right-1/2 transform translate-x-1/2"
						style={{ top: "-6rem" }}
					>
						<ImageRatio ratio="1-1" classList="rounded-50%">
							<Image src="/images/trend-4.png" alt="" loading="lazy" unsized />
						</ImageRatio>
					</div>
					<Input
						placeholder="نام کاربری"
						value=""
						type="text"
						inputClass="block w-full rounded-none mt-4 mb-3"
					/>
					<Input
						placeholder="ایمیل"
						value=""
						type="text"
						inputClass="block w-full rounded-none  mb-3"
					/>
					<Input
						placeholder="پسوورد"
						value=""
						type="text"
						inputClass="block w-full rounded-none  mb-3"
					/>
					<button className="btn btn-wide mt-12 py-3" type="submit">
						ثبت
					</button>
					<div className="text-sm mt-2">
						آیا اکانت دارید؟
						<Link href="/signin">
							<a className="text-brand-1 px-2">ورود</a>
						</Link>
					</div>
				</form>
			</SectionContainer>
		</CenterLayout>
	);
};

export default Signup;
