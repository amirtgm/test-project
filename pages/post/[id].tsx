import { useSelector } from "react-redux";
import storeWrapper, { RootState } from "../../store";
import { Layout, SectionContainer } from "../../Components";
import { fetchPostById } from "actions";

const PostPage = (): JSX.Element => {
	const postState = useSelector<RootState, PostState>((state) => state.post);

	return (
		<Layout
			headerPosition="initial"
			pageTitle={`Games Hubs | Game Page ${postState.post.title}`}
		>
			<SectionContainer classList="mb-10 ">
				<h1>post page</h1>
				<p>{postState.post.title}</p>
				<p>{postState.post.content}</p>
			</SectionContainer>
		</Layout>
	);
};
export const getServerSideProps = storeWrapper.getServerSideProps(
	async ({ store, req, res, query, ...etc }) => {
		await store.dispatch<any>(fetchPostById(query.id));
	}
);
export default PostPage;
