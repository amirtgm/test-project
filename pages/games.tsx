import React, { useState } from "react";
import Link from "next/link";
import Image from "next/image";
import Layout from "../Components/Layout/Layout";
import { SectionContainer, FilterBox, ImageRatio } from "../Components";
import storeWrapper from "../store";
import { fetchGames } from "../store/actions";
import { useSelector } from "react-redux";
import { RootState } from "../store/reducers";

const filters = ["جدیدتر", "قدیمی‌تر", "بیشترین بازدید", "کمترین بازدید"];
const posts = (): JSX.Element => {
	const gamesState = useSelector<RootState, GamesState>((state) => state.games);
	const [selectedFilter, setFilter] = useState<string>(filters[0]);

	return (
		<Layout headerPosition="initial" pageTitle="Games Hubs | Posts">
			<SectionContainer classList="container mx-auto px-4 relative mb-10">
				<FilterBox
					filterItem={filters}
					selectedFilter={selectedFilter}
					setFilterHandler={setFilter}
				/>
			</SectionContainer>
			<SectionContainer classList="container mx-auto px-4 relative mb-10">
				<ul className="-mx-4">
					{gamesState.games.map((game, index) => (
						<li
							key={index}
							className="xl:w-1/5 lg:w-1/4 md:w-2/6 w-2/4 px-4 float-right mb-4"
						>
							<Link href={`/game/${game.id}`}>
								<a>
									<ImageRatio classList="mt-3 sm:mt-0" ratio="poster">
										<Image
											className="w-full"
											// src={`/${post.imgUrl}`}
											src="/images/trend-4.png"
											alt="GameImage"
											unsized
											loading="lazy"
										/>
									</ImageRatio>
								</a>
							</Link>
						</li>
					))}
				</ul>
			</SectionContainer>
		</Layout>
	);
};
export const getServerSideProps = storeWrapper.getServerSideProps(
	async ({ store, req, res, ...etc }) => {
		await store.dispatch<any>(fetchGames());
	}
);

export default posts;
