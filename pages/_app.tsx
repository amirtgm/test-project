import App from "next/app";
import React from "react";
import Router from "next/router";
import NProgress from "nprogress";
import "nprogress/nprogress.css";
import storeWrapper from "../store";
import "../styles/main.scss";
import "../public/fonts/fonts.css";
import "../public/fonts/iconFont/iconHub.css";

//Binding events.
Router.events.on("routeChangeStart", () => NProgress.start());
Router.events.on("routeChangeComplete", () => NProgress.done());
Router.events.on("routeChangeError", () => NProgress.done());

class MyApp extends App {
	render() {
		const { Component, pageProps } = this.props;
		return <Component {...pageProps}></Component>;
	}
}

export default storeWrapper.withRedux(MyApp);
