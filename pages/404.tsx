import React from "react";
import Image from "next/image";
import CenterLayout from "../Components/Layout/CenterLayout";
import { SectionContainer } from "../Components";

const Custom404 = (): JSX.Element => {
	return (
		<CenterLayout headerPosition="initial" pageTitle="Games Hubs | 404">
			<SectionContainer classList="container mx-auto px-4 relative">
				<div className="w-full sm:w-448 mx-auto rounded-8 rounded-t-none border-3 border-white px-8 pt-16 pb-24 text-center relative">
					<div
						className="absolute right-1/2 transform translate-x-1/2 w-full px-8"
						style={{ top: "-78px" }}
					>
						<h2 className="text-5xl text-center">صفحه مورد نظر یافت نشد!‍</h2>
					</div>
					<h1 className="text-center text-8xl mb-12 text-stroke text-body font-black">
						404
					</h1>
					<div
						className="absolute right-1/2 transform translate-x-1/2 w-64"
						style={{ bottom: "-50px" }}
					>
						<Image src="/errors/404.svg" className="w-full" unsized loading="lazy" />
					</div>
				</div>
			</SectionContainer>
		</CenterLayout>
	);
};

export default Custom404;
