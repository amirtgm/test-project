import React, { useEffect, useState } from "react";
import Layout from "../Components/Layout/Layout";
import { SectionContainer, FilterBox, NewsRowItem } from "../Components";
import storeWrapper from "../store";
import { fetchPosts } from "../store/actions";
import { useSelector } from "react-redux";
import { RootState } from "../store/reducers";

const filters = ["جدیدتر", "قدیمی‌تر", "بیشترین بازدید", "کمترین بازدید"];

const News = (): JSX.Element => {
	const postsState = useSelector<RootState, PostsState>((state) => state.posts);
	const [selectedFilter, setFilter] = useState<string>(filters[0]);
	return (
		<Layout headerPosition="initial" pageTitle="Games Hubs | News">
			<SectionContainer classList="container mx-auto px-4 relative mb-10">
				<FilterBox
					filterItem={filters}
					selectedFilter={selectedFilter}
					setFilterHandler={setFilter}
				/>
			</SectionContainer>
			<SectionContainer classList="container mx-auto px-4 relative mb-10">
				<ul>
					{postsState.posts.map((item) => (
						<li key={item.id} className="mb-6">
							<NewsRowItem {...item} imgUrl="images/cat-1.png" />
						</li>
					))}
				</ul>
			</SectionContainer>
		</Layout>
	);
};
export const getServerSideProps = storeWrapper.getServerSideProps(
	async ({ store, req, res, ...etc }) => {
		await store.dispatch<any>(fetchPosts());
	}
);

export default News;
