import React from "react";
import storeWrapper, { RootState } from "../../store";
import {
	Layout,
	SectionContainer,
	GameVitrine,
	GameTabs,
	Comments,
	CommentForm,
	TrendSlider,
} from "../../Components";
import { fetchGameById } from "actions";
import { useSelector } from "react-redux";

const GamePage = (): JSX.Element => {
	const gameState = useSelector<RootState, GameState>((state) => state.gamePage);

	return (
		<Layout
			headerPosition="initial"
			pageTitle={`Games Hubs | Game Page ${gameState.gamePage.game.title}`}
		>
			<SectionContainer classList="mb-10 ">
				<GameVitrine />
			</SectionContainer>
			<SectionContainer
				classList="container mx-auto px-4 relative mb-10"
				sectionTitle="درباره بازی"
				titlePadding="pb-3"
			>
				<p className="text-justify">{gameState.gamePage.game.content}</p>
			</SectionContainer>
			<SectionContainer classList="container mx-auto px-4 mb-10 ">
				<GameTabs />
			</SectionContainer>
			<SectionContainer
				classList="container mx-auto px-4 mb-10 "
				sectionTitle="نظرات"
				hasBorder={true}
			>
				<Comments />
			</SectionContainer>
			<SectionContainer classList="container mx-auto px-4 mb-10 ">
				<CommentForm />
			</SectionContainer>
			<SectionContainer
				classList="container mx-auto px-4 mb-10 "
				sectionTitle="بازی های مشابه"
				hasBorder={true}
			>
				<>
					<TrendSlider trendList={gameState.gamePage.games} />
				</>
			</SectionContainer>
		</Layout>
	);
};
export const getServerSideProps = storeWrapper.getServerSideProps(
	async ({ store, req, res, query, ...etc }) => {
		await store.dispatch<any>(fetchGameById(query.id));
	}
);
export default GamePage;
