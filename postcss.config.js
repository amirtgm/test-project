const purgecss = [
	"@fullhuman/postcss-purgecss",
	{
		content: ["./Components/**/*.js", "./pages/**/*.js", "./style/**/*.scss"],
		defaultExtractor: (content) => content.match(/[\w-/:]+(?<!:)/g) || [],
	},
];
module.exports = {
	plugins: [
		"tailwindcss",
		"autoprefixer",
		// ...(process.env.NODE_ENV === "production" ? [purgecss] : []),
	],
};
