module.exports = {
	// purge: {
	// 	content: [
	// 		"./components/**/*.{js,ts,jsx,tsx}",
	// 		"./pages/**/*.{js,ts,jsx,tsx}",
	// 	],
	// 	options: {
	// 		extractors: [
	// 			{
	// 				extractor: content => content.match(/[A-Za-z0-9-_:\/]+/g) || [],
	// 				extensions: ["css", "html", "vue", "tsx", "js", "jsx"],
	// 			},
	// 		],
	// 	}
	// },
	theme: {
		extend: {
			colors: {
				body: "#1B1C1A",
				"dark-bg": "#151514",
				"dark-gray": "#5F615D",
				"brand-1": "#F24C49",
				"brand-2": "#ffa372",
				"brand-3": "#0F4C81",
				overlay: "#272727",
			},
			fontSize: {
				"7xl": "5rem",
				"8xl": "6rem",
			},
			opacity: {
				90: "0.90",
			},
			zIndex: {
				"-10": "-10",
			},
			borderColor: {
				"dark-bg": "#151514",
			},
			borderRadius: {
				8: "8px",
				"50%": "50%",
			},
			borderWidth: {
				3: "3px",
			},
			inset: {
				"1/2": "50%",
			},
			lineHight: {
				12: "48px",
			},
			width: {
				"3/4": "75%",
				448: "448px",
			},
			height: {
				18: "72px",
			},
			minHeight: {
				24: "96px",
			},
			inset: {
				4: "16px",
				2: "8px",
				"1/2": "50%",
			},
			margin: {
				100: "100px",
			},
		},
	},
	variants: {},
	plugins: [],
};
