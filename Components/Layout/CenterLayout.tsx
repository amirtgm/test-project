import * as React from "react";
import Head from "next/head";
import { Header, Footer } from "../";

export interface ILayoutProps {
  children: React.ReactElement | React.ReactElement[];
  headerPosition?: string;
  pageTitle?: string;
}

const Layout = ({
  children,
  headerPosition = "absolute",
  pageTitle,
}: ILayoutProps): JSX.Element => {
  return (
    <>
      <Head>
        <title>{pageTitle}</title>
      </Head>
      <div className="min-h-screen flex flex-col">
        <Header headerPosition={headerPosition} />
        <div style={{ minHeight: "calc(100vh - 88px - 175px)" }}>
          <div className="flex-grow flex flex-col justify-center h-full p-12">
            {children}
          </div>
        </div>
        <Footer />
      </div>
    </>
  );
};
export default Layout;
