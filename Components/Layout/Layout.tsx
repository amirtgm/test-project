import * as React from "react";
import Head from "next/head";
import { Header, Footer } from "../";

export interface ILayoutProps {
	children: React.ReactElement | React.ReactElement[];
	headerPosition?: string;
	pageTitle?: string;
}

const Layout = ({
	children,
	headerPosition = "absolute",
	pageTitle,
}: ILayoutProps): JSX.Element => {
	return (
		<>
			<Head>
				<title>{pageTitle}</title>
			</Head>
			<div className="min-h-screen flex flex-col">
				<Header headerPosition={headerPosition} />
				<div
					className={`flex-grow ${
						headerPosition === "absolute" ? "lg:mt-0 mt-100" : ""
					}`}
				>
					{children}
				</div>
				<Footer />
			</div>
		</>
	);
};
export default Layout;
