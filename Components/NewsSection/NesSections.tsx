import * as React from "react";
import Image from "next/image";
import Link from "next/link";
import { ImageRatio } from "../";

export interface INewsSectionProps {
	newsList: IPosts[];
}

const NewsSection = ({ newsList }: INewsSectionProps): JSX.Element => {
	return (
		<div className="flex flex-wrap">
			<div className="lg:w-1/2 w-full lg:mb-0 mb-4">
				<Link href={`/post/${newsList[0]?.id}`}>
					<a>
						<ImageRatio
							classList="rounded-lg mt-3 sm:mt-0"
							ratio="1-1"
							isOverlay={true}
							overlayText={newsList[0]?.content}
						>
							<>
								<Image
									className="w-full"
									src="/images/RingofElysium.png"
									alt="CatImage"
									unsized
									loading="lazy"
								/>
							</>
						</ImageRatio>
					</a>
				</Link>
			</div>
			<div className="lg:w-1/2 w-full lg:pr-4 pr-0 ">
				<Link href={`/post/${newsList[1]?.id}`}>
					<a>
						<ImageRatio
							classList="rounded-lg mt-3 sm:mt-0"
							ratio="16-9"
							isOverlay={true}
							overlayText={newsList[1]?.title}
						>
							<Image
								className="w-full"
								src="/images/RingofElysium.png"
								alt="GameImage"
								unsized
								loading="lazy"
							/>
						</ImageRatio>
					</a>
				</Link>
				<div className="sm:flex mt-2 md:mt-4">
					<div className="sm:w-3/5 w-full ml-2 ">
						<Link href={`/post/${newsList[2]?.id}`}>
							<a>
								<ImageRatio
									classList="rounded-lg mt-3 sm:mt-0"
									ratio="4-3"
									isOverlay={true}
									overlayText={newsList[2]?.title}
								>
									<Image
										className="w-full"
										src="/images/RingofElysium.png"
										alt="GameImage"
										unsized
										loading="lazy"
									/>
								</ImageRatio>
							</a>
						</Link>
					</div>
					<div className="sm:w-2/5 w-full sm:mr-2">
						<Link href={`/post/${newsList[3]?.id}`}>
							<a>
								<ImageRatio
									classList="rounded-lg mt-3 sm:mt-0"
									ratio="ver"
									isOverlay={true}
									overlayText={newsList[3]?.title}
								>
									<Image
										className="w-full"
										src="/images/RingofElysium.png"
										alt="GameImage"
										unsized
										loading="lazy"
									/>
								</ImageRatio>
							</a>
						</Link>
					</div>
				</div>
			</div>
		</div>
	);
};

export default NewsSection;
