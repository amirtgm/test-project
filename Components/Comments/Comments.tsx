import React from "react";
import Comment from "./Comment";
import { userInfo } from "os";
const commentsList = [
	{
		id: 1,
		img: "",
		user: "مهدی",
		content:
			"لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با استفاده از طراحان گرافیک است، چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است، و برای شرایط فعلی تکنولوژی مورد نیاز، و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد، کتابهای زیادی در شصت و سه درصد گذشته حال و آینده، شناخت فراوان جامعه و متخصصان را می طلبد، تا با نرم افزارها شناخت بیشتری را برای طراحان رایانه ای علی الخصوص طراحان خلاقی، و فرهنگ پیشرو در زبان فارسی ایجاد کرد، در این صورت می توان امید داشت که تمام و دشواری موجود در ارائه راهکارها،",
		like: 10,
		reply: [
			{
				user: "جعفر",
				id: "1.1",
				img: "",
				content:
					"لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با استفاده از طراحان گرافیک است، چاپگرها و متون بلکه روزنامه و مجله لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با استفاده از طراحان گرافیک است، چاپگرها و متون بلکه روزنامه و مجله",
				like: 1,
			},

			{ user: "رضا", id: "1.2", img: "", content: "جواب دوم کامت اول", like: 4 },
		],
	},
	{ user: "ساسان", id: 2, img: "", content: "کامنت ۲", reply: [], like: 5 },
	{ user: "نگار", id: 3, img: "", content: "کامنت ۳", reply: [], like: 9 },
	{ user: "بی نام", id: 4, img: "", content: "کامنت ۴", reply: [], like: 0 },
];
const Comments = (): JSX.Element => {
	return (
		<ul>
			{commentsList.map((comment) => {
				return (
					<li key={comment.id} className="mb-4">
						<Comment
							userName={comment.user}
							userImg={comment.img}
							comment={comment.content}
							like={comment.like}
						/>
						{comment.reply.length > 0 && (
							<ul className="mr-20 ml-24 mt-8 mb-12">
								{comment.reply.map((comment) => {
									return (
										<li
											key={comment.id}
											className="mb-4 border-brand-3 border-2 rounded-lg p-2"
										>
											<Comment
												userName={comment.user}
												userImg={comment.img}
												comment={comment.content}
												like={comment.like}
											/>
										</li>
									);
								})}
							</ul>
						)}
					</li>
				);
			})}
		</ul>
	);
};

export default Comments;
