import React from "react";
import Image from "next/image";

interface ICommentProps {
	userImg?: string | boolean;
	userName: string;
	comment: string;
	like: number;
}
const Comment = ({
	userImg,
	userName,
	comment,
	like,
}: ICommentProps): JSX.Element => {
	userImg = !userImg && "/icons/user-placeholder.svg";
	return (
		<div className="comment flex">
			<div className="w-16 h-16 ml-6 rounded-full">
				<Image
					loading="lazy"
					width="64"
					height="64"
					className="w-full h-full"
					src={userImg as string}
				/>
			</div>
			<div className="ml-6 comment--content">
				<h6 className="text-brand-1 text-sm">{userName}</h6>
				<p className="text-justify text-sm">{comment}</p>
			</div>
			<div className="w-16">
				<div className="w-6 h-6 float-right ml-4">
					<button>
						<i className="icon-reply"></i>
					</button>
				</div>
				<div className="w-6 h-6 float-left text-center">
					<button>
						<i className="icon-like"></i>
					</button>
					<span>{like}</span>
				</div>
			</div>
		</div>
	);
};

export default Comment;
