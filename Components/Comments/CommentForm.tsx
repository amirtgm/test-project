import React from "react";
import { Button, Input } from "../";

const CommentForm = (): JSX.Element => {
	return (
		<form>
			<h4 className="mb-4">نظر شما</h4>
			<textarea
				className="rounded-lg border-brand-1 border w-full h-24 text-dark-gray bg-body p-2 placeholder-dark-gray placeholder-opacity-50 min-h-24 overflow-hidden"
				placeholder="نظر شما"
			></textarea>
			<div>
				<Input
					value=""
					type="text"
					placeholder="ایمیل"
					inputClass="w-3/4 border border-3 placeholder-dark-gray"
				/>
				<Button label="ارسال نظر" classList="btn-wide float-left leading-12 h-12" />
			</div>
		</form>
	);
};

export default CommentForm;
