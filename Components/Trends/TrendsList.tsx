import React, { useState } from "react";
import Trend from "./Trend";

const trneds = ["ترند 1", "ترند 2", "ترند 3", "ترند 4", "ترند 5", "ترند 6"];

const TrendsList = (): JSX.Element => {
	const [activeTrend, setActiveTrend] = useState<number>(0);
	return (
		<ul
			className="flex justify-center flex-wrap lg:absolute top-0 "
			style={{ left: 120 + "px", right: 120 + "px" }}
		>
			{trneds.map((el, index) => (
				<li
					onClick={() => setActiveTrend(index)}
					key={index}
					className="mx-2 cursor-pointer  mb-2"
				>
					<Trend title={el} isActive={index === activeTrend} />
				</li>
			))}
		</ul>
	);
};

export default TrendsList;
