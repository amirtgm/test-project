import React from "react";
import Image from "next/image";
import { ImageRatio } from "../";
interface ITrendCardProps {
	src: string;
	title: string | undefined;
}
const TrendCard = ({ src, title }: ITrendCardProps): JSX.Element => {
	return (
		<div className="relative">
			<div className="absolute top-0 right-0 bg-brand-1 z-10 px-1 py-2 rounded-b-full cursor-pointer">
				<Image src="/icons/star.svg" unsized loading="lazy" />
			</div>
			<ImageRatio classList="mt-3 sm:mt-0" ratio="poster">
				<Image
					className="w-full"
					src={src}
					alt="GameImage"
					unsized
					loading="lazy"
				/>
			</ImageRatio>
			<h6 className="text-brand-1 text-sm">{title}</h6>
			<p className="text-brand-1 text-xs"> نمره : 5.5</p>
		</div>
	);
};

export default TrendCard;
