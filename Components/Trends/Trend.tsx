import React from "react";

interface ITrendProps {
	title: string;
	isActive: boolean;
}
const Trend = ({ title, isActive }: ITrendProps): JSX.Element => {
	return (
		<div
			className={`${
				isActive ? "bg-brand-1" : "bg-overlay"
			} lg:px-8 px-4  leading-8  rounded-full text-sm`}
		>
			{title}
		</div>
	);
};

export default Trend;
