import React from "react";
import { Slider } from "../";
import { SwiperSlide } from "swiper/react";
import TrendCard from "./TrendCard";

const params = {
	centeredSlides: true,
	loop: true,
	slidesPerView: 7.6,
	spaceBetween: 20,
	navigation: {},
	breakpoints: {
		0: {
			slidesPerView: 2,
			centeredSlides: false,
		},
		480: {
			slidesPerView: 3,
			centeredSlides: false,
		},
		640: {
			slidesPerView: 4,
			centeredSlides: false,
		},
		1024: {
			slidesPerView: 5.6,
		},
		1280: {
			slidesPerView: 7.6,
		},
	},
};
interface ITrendSliderProps {
	trendList: IGames[];
}
const TrendSlider = ({ trendList }: ITrendSliderProps): JSX.Element => {
	return (
		<div>
			<Slider params={params}>
				{trendList.map((trend, index) => (
					<SwiperSlide key={trend.id}>
						<TrendCard src={`/images/trend-${index}.png`} title={trend.title} />
					</SwiperSlide>
				))}
			</Slider>
		</div>
	);
};

export default TrendSlider;
