import * as React from "react";
import { ImageRatio, Button, Input } from "../";

export interface IShowcaseProps {}

const Showcase = (props: IShowcaseProps): JSX.Element => {
	return (
		<div className="showcase">
			<div className="showcase--photo w-full relative">
				<div
					className="w-full absolute bg-center h-full bg-cover bg-no-repeat -z-10"
					style={{ backgroundImage: `url(/images/showcase.png)` }}
				></div>
				<div className="container mx-auto px-16 mb-10 pt-24 pb-32 h-full">
					<div className="rounded-8 border-3 border-brand-1 w-full h-full relative">
						<div className="showcase--side-box">
							<div className="showcase--side-box--title relative">
								<h3 className="transform -rotate-90 -translate-y-1/2 absolute top-1/2">
									عنوان ۱
								</h3>
							</div>
							<p className="showcase--side-box--text">
								لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ لورم ایپسوم
								متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ
							</p>
						</div>
						<div className="showcase--bottom-box">
							<h3 className="showcase--bottom-box--title">عنوان ۲</h3>
							<p className="showcase--bottom-box--text">
								لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ لورم ایپسوم
								متن ساختگی با تولید سادگی
							</p>
						</div>
					</div>
					<form className="mt-10 w-3/4 float-right">
						<Input
							value=""
							type="text"
							placeholder="جتسجو"
							inputClass="w-3/4 border-3"
						/>
						<Button
							label="جستجو‍‍‍"
							classList="btn-wide float-left leading-12 h-12"
						/>
					</form>
				</div>
			</div>
		</div>
	);
};

export default Showcase;
