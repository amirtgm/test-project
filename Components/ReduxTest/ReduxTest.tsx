/* eslint-disable react/prop-types */
import React, { useRef, useEffect } from "react";
import { connect, ConnectedProps, useSelector } from "react-redux";
import { fetchPosts } from "../../store/actions/posts.action";
import { AnyAction, bindActionCreators, Dispatch } from "redux";

const mapState = (state: { posts: PostsState }) => {
	return {
		posts: state.posts,
	};
};
const mapDispatch = (dispatch: Dispatch<AnyAction>) => {
	return {
		fetchPosts: bindActionCreators(fetchPosts, dispatch),
	};
};
const connector = connect(mapState, mapDispatch);
type IReduxTest = ConnectedProps<typeof connector>;

const ReduxTest = ({ posts, fetchPosts }: IReduxTest) => {
	const ref = useRef();
	useEffect(() => {
		console.log(posts);
	}, [posts]);
	const handelDate = () => {
		fetchPosts();
	};
	return (
		<div className="mt-24">
			<h1 onClick={ref.current}>Redux Test</h1>
			<button className="block btn mb-4" onClick={handelDate}>
				get posts
			</button>
			<h2>isLoadingPosts: {posts.loading?.toString()}</h2>
			<hr />
			<h3 className="text-left">: posts list</h3>
			<div>{JSON.stringify(posts.posts, null, 2)}</div>
		</div>
	);
};
export default connector(ReduxTest);
