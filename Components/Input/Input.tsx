import React, { useState } from "react";

interface IInputProps {
	label?: string;
	value: string;
	type: string;
	placeholder?: string;
	labelClass?: string;
	inputClass?: string;
}
const Input = ({
	label,
	value,
	type,
	placeholder,
	labelClass,
	inputClass,
}: IInputProps): JSX.Element => {
	const [inputValue, setValue] = useState<string>(value);
	return (
		<React.Fragment>
			{label && <label className={`ml-3 leading-12 ${labelClass}`}>{label}</label>}
			<input
				className={`rounded-8 border border-brand-1 bg-transparent h-12 leading-12 px-2 placeholder-white placeholder-opacity-50 ${inputClass}`}
				value={inputValue}
				type={type}
				onChange={(e) => setValue(e.target.value)}
				placeholder={placeholder}
			/>
		</React.Fragment>
	);
};

export default Input;
