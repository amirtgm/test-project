import React from "react";
import Image from "next/image";
import { ImageRatio } from "..";
import Link from "next/link";

const NewsRowItem = ({ title, content, imgUrl, id }: IPosts): JSX.Element => {
	return (
		<div className="row-item border-2 border-brand-1 rounded-lg overflow-hidden pl-4  relative">
			<div className="row-item--img md:w-1/5 w-2/5 float-right">
				<Link href={`/post/${id}`}>
					<a>
						<ImageRatio ratio="1-1" classList="rounded-lg">
							<Image
								src={`/${imgUrl}`}
								className="w-full"
								alt={title}
								unsized
								loading="lazy"
							/>
						</ImageRatio>
					</a>
				</Link>
			</div>
			<div className="row-item--content md:w-4/5 w-3/5 float-right pr-2">
				<Link href={`/post/${id}`}>
					<a>
						<h3 className="row-item--content--title lg:mt-8 lg:mb-4 mt-4 mb-2 text-xl md:text-2xl">
							{title}
						</h3>
					</a>
				</Link>
				<p className="row-item--content--desc text-justify  mb-6 overflow-hidden xl:h-32 lg:h-16 md:h-10 h-18 md:text-sm text-xs">
					{content}
				</p>
			</div>
			<div className="border-2 border-brand-1 rounded-full absolute bottom-2 left-2">
				<Link href={`/post/${id}`}>
					<a className="w-full text-xs py-2 px-6">ادامه</a>
				</Link>
			</div>
		</div>
	);
};

export default NewsRowItem;
