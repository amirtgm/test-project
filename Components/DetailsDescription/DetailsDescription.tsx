import React, { FC } from "react";

export interface IDetailsDescriptionProps {
	classList?: string;
}

const DetailsDescription: FC<IDetailsDescriptionProps> = ({
	classList,
}: IDetailsDescriptionProps): JSX.Element => {
	return (
		<div className={classList}>
			<h1 className="py-4 text-lg font-bold">عنوان اصلی صفحه داخلی بازی</h1>
			<p className="text-sm text-justify">
				لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و بلورم ایپسوم متن
				ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.
				چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای
				شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای
				کاربردی می باشد. کتابهای زیادی در شصت و سه درصد گذشته، حال و آینده شناخت
				فراوان جامعه و متخصصان را می طلبد تا با نرم افزارها شناخت بیشتری را برای
				طراحان رایانه ای علی الخصوص طراحان خلاقی و فرهنگ پیشرو در زبان فارسی ایجاد
				کرد.
			</p>
		</div>
	);
};

export default DetailsDescription;
