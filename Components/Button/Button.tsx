import React, { FC } from "react";
// import "./Button.scss";

export interface IButtonProps {
	label: string;
	classList?: string;
	handleClick?: (event: React.MouseEvent<HTMLButtonElement>) => void;
}

const Button = ({
	classList,
	handleClick,
	label,
}: IButtonProps): JSX.Element => {
	return (
		<button className={`btn ${classList}`} onClick={handleClick}>
			{label}
		</button>
	);
};
export default Button;
