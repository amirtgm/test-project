import * as React from "react";
import Image from "next/image";
import { ImageRatio } from "../";

export interface ICategoryProps {
	categoriesList: ICategory[];
}

const Categories = ({ categoriesList }: ICategoryProps): JSX.Element => {
	return (
		<ul className="grid lg:grid-cols-5 md:grid-cols-3 grid-cols-2 gap-4">
			{categoriesList &&
				categoriesList.map((item, index) => (
					<li key={item.id} className="">
						<a href="/">
							<ImageRatio classList="rounded-lg mt-3 sm:mt-0" ratio="1-1">
								<Image
									className="w-full"
									src={`/images/cat-${index + 1}.png`}
									alt="CatImage"
									unsized
									loading="lazy"
								/>
							</ImageRatio>
						</a>
					</li>
				))}
		</ul>
	);
};

export default Categories;
