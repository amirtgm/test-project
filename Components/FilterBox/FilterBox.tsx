import React, { useState, Dispatch, SetStateAction } from "react";

interface IFilterBoxProps {
	filterItem: string[];
	selectedFilter: string;
	setFilterHandler: Dispatch<SetStateAction<string>>;
}
const FilterBox = ({
	filterItem,
	selectedFilter,
	setFilterHandler,
}: IFilterBoxProps): JSX.Element => {
	return (
		<ul className="border-2 border-brand-1 rounded-t-lg flex px-4 ">
			{filterItem.map((item, index) => (
				<li
					key={index}
					style={{ margin: "-5px 0" }}
					className={`md:px-12 sm:px-8 px-4 py-4  md:text-base text-sm ${
						item === selectedFilter ? "border-b-8 border-brand-1" : ""
					}`}
				>
					<button onClick={() => setFilterHandler(item)}>{item}</button>
				</li>
			))}
		</ul>
	);
};

export default FilterBox;
