import React from "react";

//https://swiperjs.com/react/
// import Swiper core and required components
import SwiperCore, { Navigation, Pagination, Scrollbar, A11y } from "swiper";
import { Swiper } from "swiper/react";

interface SwiperNav extends Swiper {
	Navigation?: boolean;
}
export interface ISliderProps {
	children: JSX.Element[] | JSX.Element;
	params: Swiper;
}
SwiperCore.use([Navigation, Pagination, Scrollbar]);

const Slider = (props: ISliderProps): JSX.Element => {
	const { children, params } = props;
	return <Swiper {...params}>{children}</Swiper>;
};

export default Slider;
