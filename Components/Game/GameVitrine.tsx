import React from "react";
import Image from "next/image";
import { ImageRatio } from "../";

interface IGameVitrineProps {}

const GameVitrine = (props: IGameVitrineProps): JSX.Element => {
	return (
		<div className="game-vitrine ">
			<div className="game-vitrine--photo w-full relative">
				<div
					className="game-vitrine--photo--img w-full absolute bg-center h-full bg-cover bg-no-repeat -z-10 "
					style={{ backgroundImage: `url(/images/game-img.png)` }}
				></div>
				<div className="container mx-auto relative h-full">
					<div className="game-vitrine--photo--border absolute inset-0">
						<div className="rounded-8 border-3 border-brand-1 w-full h-full relative"></div>
					</div>
				</div>
			</div>
			<div className="container mx-auto mt-6 px-6">
				<ul className="flex flex-wrap justify-center">
					<li className="lg:w-1/5 w-1/3 px-2 pb-4">
						<ImageRatio classList="rounded-lg mt-3 sm:mt-0" ratio="1-1">
							<Image
								className="w-full"
								src="/images/cat-1.png"
								alt="GameImage"
								width="230.39"
								height="230.39"
								loading="lazy"
							/>
						</ImageRatio>
					</li>
					<li className="lg:w-1/5 w-1/3 px-2 pb-4">
						<ImageRatio classList="rounded-lg mt-3 sm:mt-0" ratio="1-1">
							<Image
								className="w-full"
								src="/images/cat-2.png"
								alt="GameImage"
								unsized
								loading="lazy"
							/>
						</ImageRatio>
					</li>
					<li className="lg:w-1/5 w-1/3 px-2 pb-4">
						<ImageRatio classList="rounded-lg mt-3 sm:mt-0" ratio="1-1">
							<Image
								className="w-full"
								src="/images/cat-3.png"
								alt="GameImage"
								unsized
								loading="lazy"
							/>
						</ImageRatio>
					</li>
					<li className="lg:w-1/5 w-1/3 px-2 pb-4">
						<ImageRatio classList="rounded-lg mt-3 sm:mt-0" ratio="1-1">
							<Image
								className="w-full"
								src="/images/cat-4.png"
								alt="GameImage"
								unsized
								loading="lazy"
							/>
						</ImageRatio>
					</li>
					<li className="lg:w-1/5 w-1/3 px-2 pb-4">
						<ImageRatio classList="rounded-lg mt-3 sm:mt-0" ratio="1-1">
							<Image
								className="w-full"
								src="/images/cat-5.png"
								alt="GameImage"
								unsized
								loading="lazy"
							/>
						</ImageRatio>
					</li>
				</ul>
			</div>
		</div>
	);
};

export default GameVitrine;
