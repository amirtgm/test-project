import React from "react";

const RenderTabs = (): JSX.Element => {
	const [openTab, setOpenTab] = React.useState<number>(0);
	return (
		<>
			<div className="flex flex-wrap">
				<div className="w-full">
					<ul
						className="flex mb-0 list-none flex-wrap pt-3 relative z-10 -mb-4"
						role="tablist"
					>
						<li className="mr-3 text-center" key={0}>
							<a
								className={
									"text-xs font-bold uppercase px-5 py-3 shadow-md rounded-lg rounded-b-none block leading-normal " +
									(openTab === 0
										? "text-white bg-brand-1"
										: "text-white" + " bg-dark-gray")
								}
								onClick={(e) => {
									e.preventDefault();
									setOpenTab(0);
								}}
								data-toggle="tab"
								href={`#link-0`}
								role="tablist"
							>
								لینک‌‌ها
							</a>
						</li>
						<li className="mr-3 text-center" key={1}>
							<a
								className={
									"text-xs font-bold uppercase px-5 py-3 shadow-md rounded-lg rounded-b-none block leading-normal " +
									(openTab === 1
										? "text-white bg-brand-1"
										: "text-white" + " bg-dark-gray")
								}
								onClick={(e) => {
									e.preventDefault();
									setOpenTab(1);
								}}
								data-toggle="tab"
								href={`#link-1`}
								role="tablist"
							>
								راهنما
							</a>
						</li>
						<li className="mr-3 text-center" key={2}>
							<a
								className={
									"text-xs font-bold uppercase px-5 py-3 shadow-md rounded-lg rounded-b-none block leading-normal " +
									(openTab === 2
										? "text-white bg-brand-1"
										: "text-white" + " bg-dark-gray")
								}
								onClick={(e) => {
									e.preventDefault();
									setOpenTab(2);
								}}
								data-toggle="tab"
								href={`#link-2`}
								role="tablist"
							>
								حداقل سیستم مورد نیاز
							</a>
						</li>
					</ul>
					<div className="relative flex flex-col min-w-0 break-words w-full mb-6 shadow-lg rounded-lg rounded-t-none bg-box border-brand-1 border-2 pt-4">
						<div className="px-4 py-5 flex-auto ">
							<div className="tab-content tab-space">
								<div className={openTab === 0 ? "block" : "hidden"} id="link-0" key={0}>
									لینک های دانلود
								</div>
								<div className={openTab === 1 ? "block" : "hidden"} id="link-1" key={1}>
									راهنما بازی
								</div>
								<div className={openTab === 2 ? "block" : "hidden"} id="link-2" key={2}>
									اطلاعات سیستم مورد نیاز
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</>
	);
};
const GameTabs = (): JSX.Element => {
	return <RenderTabs />;
};

export default GameTabs;
