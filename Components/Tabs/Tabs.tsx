import React, { FC } from "react";

export interface ITabsProps {
	color: string;
	defaultTab: number;
	tabContent: Array<{ title: string; content: string }>;
}

const RenderTabs = ({
	color,
	defaultTab,
	tabContent,
}: ITabsProps): JSX.Element => {
	const [openTab, setOpenTab] = React.useState(defaultTab);
	return (
		<>
			<div className="flex flex-wrap">
				<div className="w-full">
					<h3 className="float-right mt-6">عنوان تب ها</h3>
					<ul
						className="flex mb-0 list-none flex-wrap pt-3 flex-row justify-end"
						role="tablist"
					>
						{tabContent.map((item, index) => {
							return (
								<li className="mr-3 text-center" key={index}>
									<a
										className={
											"text-xs font-bold uppercase px-5 py-3 shadow-md rounded-lg rounded-b-none block leading-normal " +
											(openTab === index
												? "text-white bg-" + color
												: "text-" + color + " bg-white")
										}
										onClick={(e) => {
											e.preventDefault();
											setOpenTab(index);
										}}
										data-toggle="tab"
										href={`#link${index}`}
										role="tablist"
									>
										{item.title}
									</a>
								</li>
							);
						})}
					</ul>
					<div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-6 shadow-lg rounded-lg rounded-tl-none bg-box">
						<div className="px-4 py-5 flex-auto ">
							<div className="tab-content tab-space">
								{tabContent.map((item, index) => {
									return (
										<div
											className={openTab === index ? "block" : "hidden"}
											id={`link${index}`}
											key={index}
										>
											{item.content}
										</div>
									);
								})}
							</div>
						</div>
					</div>
				</div>
			</div>
		</>
	);
};

const Tabs = ({ color, defaultTab, tabContent }: ITabsProps): JSX.Element => {
	return (
		<RenderTabs color={color} defaultTab={defaultTab} tabContent={tabContent} />
	);
};
export default Tabs;
