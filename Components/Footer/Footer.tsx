import * as React from "react";
import Image from "next/image";
import Link from "next/link";
import { SocialMedia } from "../";

export interface IFooterProps {}

export default function Footer(props: IFooterProps): JSX.Element {
	return (
		<footer className="bg-dark-bg">
			<div className="container mx-auto px-4 py-8 ">
				<div className="grid grid-cols-12 gap-4">
					<div className="lg:col-span-4 col-span-12 flex justify-center lg:justify-start">
						<div className="lg:w-1/2 w-20">
							<Link href="/">
								<a className="block">
									<Image src={"/logo.svg"} alt="" unsized loading="lazy" />
								</a>
							</Link>
						</div>
					</div>
					<div className="lg:col-span-4 sm:col-span-6 col-span-12 px-4 my-4 sm:my-0">
						<ul className="flex content-center justify-around flex-wrap h-full">
							<li>
								<Link href="/">
									<a>خانه</a>
								</Link>
							</li>
							<li>
								<Link href="/news">
									<a>اخبار</a>
								</Link>
							</li>
							<li>
								<a href="/">ارتباط با ما</a>
							</li>
							<li>
								<a href="/">دسته بندی</a>
							</li>
						</ul>
					</div>
					<div className="lg:col-span-4 sm:col-span-6 col-span-12">
						<SocialMedia />
					</div>
				</div>
			</div>
		</footer>
	);
}
