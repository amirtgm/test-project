import * as React from "react";
import Image from "next/image";
import Link from "next/link";

export interface IHeaderProps {
	headerPosition: string;
}

const Header = ({ headerPosition }: IHeaderProps): JSX.Element => {
	const [isExpanded, toggleExpansion] = React.useState<boolean>(false);
	return (
		<header className="lg:container lg:mx-auto lg:relative ">
			<div
				className={`${headerPosition} inset-x-0 z-10 px-4 lg:bg-transparent bg-body`}
			>
				<nav className="flex justify-between lg:items-center  items-baseline ">
					<div className=" items-center justify-between flex-wrap py-5 ">
						<div className="inline-block lg:hidden">
							<button
								className="flex items-center pl-3 py-2 text-brand-1"
								onClick={() => toggleExpansion(!isExpanded)}
							>
								<i className="icon-menu text-2xl"></i>
							</button>
						</div>
						<div
							className={
								"block flex-grow lg:flex lg:items-center lg:w-auto pr-8 lg:pr-0 " +
								(isExpanded ? "block" : "hidden")
							}
						>
							<div className="lg:flex-grow text-sm leading-6">
								<Link href="/">
									<a className="block mx-8 my-3 mr-0 lg:inline-block">خانه</a>
								</Link>

								<Link href="/news">
									<a className="block mr-0 lg:mr-20 mx-8 my-3 lg:inline-block">اخبار</a>
								</Link>
								<Link href="/games">
									<a className="block mr-0 lg:mr-20 mx-8 my-3 lg:inline-block">
										بازی‌ها
									</a>
								</Link>
								<a href="/" className="block mr-0 lg:mr-20 mx-8 my-3 lg:inline-block">
									ارتباط با ما
								</a>
							</div>
						</div>
					</div>
					<div className="flex items-center py-5">
						{/* <button className="items-center pl-3 py-2 ml-8">
						<img className="w-6" src={"/icons/search.svg"} alt="" />
					</button> */}
						<button className="items-center ml-3 my-3 relative hover-trigger">
							<i className="icon-profile text-2xl"></i>
							<ul
								className="absolute text-sm left-0 w-24 hover-target z-10 "
								style={{ top: "24px" }}
							>
								<li className="bg-brand-1 border-2 border-brand-1 rounded-t-lg">
									<Link href="/signin">
										<a className="block w-full text-center  py-2 px-4">ورود</a>
									</Link>
								</li>
								<li className="bg-dark-gray border-2 border-brand-1 rounded-b-lg ">
									<Link href="/signup">
										<a className="whitespace-no-wrap block w-full text-center px-4 py-2">
											ثبت نام
										</a>
									</Link>
								</li>
							</ul>
						</button>
						<Link href="/">
							<a className="items-center">
								<Image
									className="w-24 px-1"
									src={"/logo.svg"}
									alt="logo"
									width="96"
									height="42"
								/>
							</a>
						</Link>
					</div>
				</nav>
			</div>
		</header>
	);
};
export default Header;
