import * as React from "react";

export interface ISocialMediaProps {}

const SocialMedia = (params: ISocialMediaProps): JSX.Element => {
	return (
		<ul className="flex content-center justify-around flex-wrap h-full">
			<li className="text-3xl">
				<a className="block" href="/">
					<i className="icon-instagram"></i>
				</a>
			</li>
			<li className="text-3xl">
				<a className="block" href="/">
					<i className="icon-youtube"></i>
				</a>
			</li>
			<li className="text-3xl">
				<a className="block" href="/">
					<i className="icon-twitter"></i>
				</a>
			</li>

			<li className="text-3xl">
				<a className="block" href="/">
					<i className="icon-telegram"></i>
				</a>
			</li>
		</ul>
	);
};

export default SocialMedia;
