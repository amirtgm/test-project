import * as React from "react";

export interface ISectionContainerProps {
	children: React.ReactElement | React.ReactElement[];
	classList?: string;
	sectionTitle?: string;
	linkTitle?: string;
	linkUrl?: string;
	titlePadding?: string;
	hasBorder?: boolean;
}

const SectionContainer = ({
	children,
	classList,
	sectionTitle,
	linkTitle,
	linkUrl,
	titlePadding = "pb-6",
	hasBorder = false,
}: ISectionContainerProps): JSX.Element => {
	return (
		<section className={hasBorder ? "is-full-border-top relative" : ""}>
			<div className={classList}>
				{(sectionTitle || (linkUrl && linkTitle)) && (
					<div className={`flex justify-between items-center ${titlePadding}`}>
						{sectionTitle && (
							<h2
								className={`text-2xl ${
									hasBorder ? "bg-body relative z-10 px-3 -mr-3" : ""
								}`}
							>
								{sectionTitle}
							</h2>
						)}
						{linkUrl && linkTitle && (
							<a className="text-xs" href={linkUrl}>
								{linkTitle}
							</a>
						)}
					</div>
				)}
				{children}
			</div>
		</section>
	);
};

export default SectionContainer;
