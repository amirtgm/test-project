import * as React from "react";

export interface IImageRatioProps {
	children: React.ReactElement;
	ratio: string;
	classList: string;
	isOverlay?: boolean;
	overlayText?: string;
}

const ImageRatio = ({
	isOverlay,
	ratio,
	classList,
	children,
	overlayText,
}: IImageRatioProps): JSX.Element => {
	return (
		<div
			className={`ratio-box ratio-box-${ratio} ${classList} ${
				isOverlay && overlayText ? "is-overlay-title" : ""
			}`}
		>
			<div className="ratio-box-content">
				{children}
				{isOverlay && overlayText && (
					<div className="image-info p-3">
						{/* <h3 className="text-sm mb-4">متن اصلی خبر</h3> */}
						<p className="text-xs text-justify">{overlayText}</p>
					</div>
				)}
			</div>
		</div>
	);
};

export default ImageRatio;
