import React, { FC } from "react";
import Image from "next/image";
import Slider from "../Slider/Slider";
import { ImageRatio, DetailsDescription } from "../";
import Tabs from "../Tabs/Tabs";

export interface IDetailsSliderProps {}

const DetailsSlider: FC<IDetailsSliderProps> = () => {
	const tabList: Array<{ title: string; content: string }> = [
		{
			title: "تب اولی",
			content: "محتوای تب اول",
		},
		{
			title: "تب دوم",
			content: "محتوای تب دوم",
		},
		{
			title: "تب سوم",
			content: "محتوای تب سوم",
		},
	];
	const params = {
		centeredSlides: true,
		slidesPerView: 1.6,
		loop: true,
		spaceBetween: 50,
		pagination: {
			el: ".swiper-pagination",
			clickable: true,
		},
	};
	return (
		<>
			<Slider params={params}>
				<div>
					<ImageRatio classList="rounded-lg mt-3 sm:mt-0" ratio="16-9">
						<Image
							loading="lazy"
							unsized
							className="w-full"
							src="/images/RingofElysium.png"
							alt="GameImage"
						/>
					</ImageRatio>
				</div>
				<div>
					<ImageRatio classList="rounded-lg mt-3 sm:mt-0" ratio="16-9">
						<Image
							loading="lazy"
							height="277.28"
							width="169"
							className="w-full"
							src="/images/RingofElysium.png"
							alt="GameImage"
						/>
					</ImageRatio>
				</div>
				<div>
					<ImageRatio classList="rounded-lg mt-3 sm:mt-0" ratio="16-9">
						<Image
							loading="lazy"
							height="277.28"
							width="169"
							className="w-full"
							src="/images/RingofElysium.png"
							alt="GameImage"
						/>
					</ImageRatio>
				</div>
			</Slider>
			<DetailsDescription classList="my-6" />
			<Tabs color="brand-1" defaultTab={0} tabContent={tabList} />
		</>
	);
};
export default DetailsSlider;
