import { compose } from 'redux';

export const composeEnhancers =
  (typeof window !== 'undefined' &&
    process.env.NODE_ENV === 'development' &&
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) ||
  compose;
