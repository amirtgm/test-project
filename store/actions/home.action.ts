import { createAsyncThunk } from "@reduxjs/toolkit";
import { request as gqlRequest } from "graphql-request";

export const fetchHome = createAsyncThunk("home/requestStatus", async () => {
	const url = process.env.DATA_URLS;
	const query = `
	query {
    games{
      id
      title
      content
    }
    categories(paging :{ limit: 5 }){
      id
      name
    }
    posts(paging :{ limit: 4 }){
      id
      title
      content
    }
	}
`;
	return await gqlRequest<IHome>(url, query);
});
