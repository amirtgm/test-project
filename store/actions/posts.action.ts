import { createAsyncThunk } from "@reduxjs/toolkit";
import { request as gqlRequest } from "graphql-request";

export const fetchPosts = createAsyncThunk("posts/requestStatus", async () => {
	const url = process.env.DATA_URLS;
	const query = `
	query {
    posts{
      id
      title
      content
    }
	}
`;
	return await gqlRequest<IPosts[]>(url, query);
});
