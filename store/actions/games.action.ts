import { createAsyncThunk } from "@reduxjs/toolkit";
import { request as gqlRequest } from "graphql-request";

export const fetchGames = createAsyncThunk("games/requestStatus", async () => {
	const url = process.env.DATA_URLS;
	const query = `
	query {
    games{
      id
      title
    }
	}
`;
	return await gqlRequest<IGames[]>(url, query);
});
