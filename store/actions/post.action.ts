import { createAsyncThunk } from "@reduxjs/toolkit";
import { request as gqlRequest } from "graphql-request";

export const fetchPostById = createAsyncThunk(
	"post/requestStatus",
	async (id: string | string[] | undefined) => {
		const url = process.env.DATA_URLS;
		const query = `
			query getPost{
        post(id: "${id}"){
          id
          title
          content
        }
      }
		`;
		return await gqlRequest<IPost>(url, query);
	}
);
