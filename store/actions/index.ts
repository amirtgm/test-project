import { fetchPosts } from "./posts.action";
import { fetchPostById } from "./post.action";
import { fetchGames } from "./games.action";
import { fetchGameById } from "./game.action";
import { fetchHome } from "./home.action";

export { fetchPosts, fetchPostById, fetchGames, fetchGameById, fetchHome };
