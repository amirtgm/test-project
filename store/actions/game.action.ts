import { createAsyncThunk } from "@reduxjs/toolkit";
import { request as gqlRequest } from "graphql-request";

export const fetchGameById = createAsyncThunk(
	"game/requestStatus",
	async (id: string | string[] | undefined) => {
		const url = process.env.DATA_URLS;
		const query = `
      query getGame{
        games(paging :{ limit: 5 }){
          id
          title
          content
        }
        game(id: "${id}"){
          id
          title
          content
          
          comments{
            id
            content
            user{
              id
              username
              isActive
            }
          }
        }
      }
    `;
		return await gqlRequest<GamePage>(url, query);
	}
);
