interface IGame {
	id?: string;
	title?: string;
	content?: string;
	comments?: IComment[];
}
interface GamePage {
	game: IGame;
	games: IGames[];
}
interface GameState {
	gamePage: GamePage;
	loading: boolean;
	error: string | unknown;
}
