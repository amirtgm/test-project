interface IGames {
	id?: string;
	title?: string;
	content?: string;
}

interface GamesState {
	games: IGame[];
	loading: boolean;
	error: string | unknown;
}
