interface IPosts {
	id?: string;
	title?: string;
	imgUrl?: string;
	content?: string;
}

interface PostsState {
	posts: IPosts[];
	loading: boolean;
	error: string | unknown;
}
