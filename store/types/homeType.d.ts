interface IHome {
	games: IGames[];
	categories: ICategory[];
	posts: IPosts[];
}

interface HomeState {
	home: IHome;
	loading: boolean;
	error: string | unknown;
}
