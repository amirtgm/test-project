interface IPost {
	id?: string;
	title?: string;
	imgUrl?: string;
	content?: string;
}

interface PostState {
	post: IPost;
	loading: boolean;
	error: string | unknown;
}
