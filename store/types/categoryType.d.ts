interface ICategory {
	id: string;
	name: string;
	image: string;
	description: string;
}
