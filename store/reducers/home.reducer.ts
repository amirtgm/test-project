import { createSlice, ActionCreatorWithPayload } from "@reduxjs/toolkit";
import { HYDRATE } from "next-redux-wrapper";
import { fetchHome } from "../actions";
import { RootState } from ".";

const initialState: HomeState = {
	home: {
		games: [],
		categories: [],
		posts: [],
	},
	loading: false,
	error: "",
};

const homeSlice = createSlice({
	name: "home",
	initialState,
	reducers: {},
	extraReducers: (builder) => {
		builder.addCase(fetchHome.pending, (state, { payload }) => {
			state.loading = true;
		});
		builder.addCase(fetchHome.fulfilled, (state, { payload }) => {
			state.home = payload;
			state.loading = false;
		});
		builder.addCase(fetchHome.rejected, (state, { payload }) => {
			state.loading = false;
			state.error = payload;
		});
		builder.addCase(
			(HYDRATE as unknown) as ActionCreatorWithPayload<RootState, string>,
			(state, { payload }) => {
				state.loading = false;
				state.home = payload.home.home;
			}
		);
	},
});

export default homeSlice;
