import { createSlice, ActionCreatorWithPayload } from "@reduxjs/toolkit";
import { HYDRATE } from "next-redux-wrapper";
import { fetchPosts } from "../actions";
import { RootState } from "../reducers";

const initialState: PostsState = {
	posts: [],
	loading: false,
	error: "",
};

const postsSlice = createSlice({
	name: "posts",
	initialState,
	reducers: {},
	extraReducers: (builder) => {
		builder.addCase(fetchPosts.pending, (state, { payload }) => {
			state.loading = true;
		});
		builder.addCase(fetchPosts.fulfilled, (state, { payload }) => {
			state.posts = payload;
			state.loading = false;
		});
		builder.addCase(fetchPosts.rejected, (state, { payload }) => {
			state.loading = false;
			state.error = payload;
		});
		builder.addCase(
			(HYDRATE as unknown) as ActionCreatorWithPayload<RootState, string>,
			(state, { payload }) => {
				state.loading = false;
				state.posts = payload.posts.posts;
			}
		);
	},
});

export default postsSlice;
