import { createSlice, ActionCreatorWithPayload } from "@reduxjs/toolkit";
import { HYDRATE } from "next-redux-wrapper";
import { fetchGameById } from "../actions";
import { RootState } from "../reducers";

const initialState: GameState = {
	gamePage: {
		game: { title: "", id: "", content: "", comments: [] },
		games: [],
	},
	loading: false,
	error: "",
};

const gameSlice = createSlice({
	name: "gamePage",
	initialState,
	reducers: {},
	extraReducers: (builder) => {
		builder.addCase(fetchGameById.pending, (state, { payload }) => {
			state.loading = true;
		});
		builder.addCase(fetchGameById.fulfilled, (state, { payload }) => {
			state.gamePage = payload;
			state.loading = false;
		});
		builder.addCase(fetchGameById.rejected, (state, { payload }) => {
			state.loading = false;
			state.error = payload;
		});
		builder.addCase(
			(HYDRATE as unknown) as ActionCreatorWithPayload<RootState, string>,
			(state, { payload }) => {
				state.loading = false;
				state.gamePage = payload.gamePage.gamePage;
			}
		);
	},
});

export default gameSlice;
