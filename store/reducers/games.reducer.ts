import { createSlice, ActionCreatorWithPayload } from "@reduxjs/toolkit";
import { HYDRATE } from "next-redux-wrapper";
import { fetchGames } from "../actions";
import { RootState } from "../reducers";

const initialState: GamesState = {
	games: [],
	loading: false,
	error: "",
};

const gamesSlice = createSlice({
	name: "games",
	initialState,
	reducers: {},
	extraReducers: (builder) => {
		builder.addCase(fetchGames.pending, (state, { payload }) => {
			state.loading = true;
		});
		builder.addCase(fetchGames.fulfilled, (state, { payload }) => {
			state.games = payload;
			state.loading = false;
		});
		builder.addCase(fetchGames.rejected, (state, { payload }) => {
			state.loading = false;
			state.error = payload;
		});
		builder.addCase(
			(HYDRATE as unknown) as ActionCreatorWithPayload<RootState, string>,
			(state, { payload }) => {
				state.loading = false;
				state.games = payload.games.games;
			}
		);
	},
});

export default gamesSlice;
