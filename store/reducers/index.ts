import { combineReducers } from "redux";
import posts from "./posts.reducer";
import post from "./post.reducer";
import games from "./games.reducer";
import game from "./game.reducer";
import home from "./home.reducer";

const rootReducer = combineReducers({
	posts: posts.reducer,
	post: post.reducer,
	games: games.reducer,
	gamePage: game.reducer,
	home: home.reducer,
});
export type RootState = ReturnType<typeof rootReducer>;

export default rootReducer;
