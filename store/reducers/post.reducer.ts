import { createSlice, ActionCreatorWithPayload } from "@reduxjs/toolkit";
import { HYDRATE } from "next-redux-wrapper";
import { fetchPostById } from "../actions";
import { RootState } from ".";

const initialState: PostState = {
	post: {
		id: "",
		title: "",
		imgUrl: "",
		content: "",
	},
	loading: false,
	error: "",
};

const postSlice = createSlice({
	name: "post",
	initialState,
	reducers: {},
	extraReducers: (builder) => {
		builder.addCase(fetchPostById.pending, (state, { payload }) => {
			state.loading = true;
		});
		builder.addCase(fetchPostById.fulfilled, (state, { payload }) => {
			state.post = payload;
			state.loading = false;
		});
		builder.addCase(fetchPostById.rejected, (state, { payload }) => {
			state.loading = false;
			state.error = payload;
		});
		builder.addCase(
			(HYDRATE as unknown) as ActionCreatorWithPayload<RootState, string>,
			(state, { payload }) => {
				state.loading = false;
				state.post = payload.post.post;
			}
		);
	},
});

export default postSlice;
