import { configureStore } from "@reduxjs/toolkit";
import { createWrapper, MakeStore, Context } from "next-redux-wrapper";
import reducer from "./reducers";
import logger from "redux-logger";

// And use redux-batch as an example of adding enhancers

const store = configureStore({
	reducer,
	middleware: (getDefaultMiddleware) => getDefaultMiddleware(),
	devTools: false,
});
export type RootState = ReturnType<typeof store.getState>;

// export store singleton instance
const makeStore: MakeStore = (context: Context) => store;

// const wrapper = createWrapper(makeStore);
const wrapper = createWrapper(makeStore, { debug: true });

export default wrapper;
