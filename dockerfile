FROM node:alpine
ENV NODE_ENV=development
ENV PATH $PATH:/usr/src/app/node_modules/.bin

WORKDIR /usr/src/app

COPY package.json ./
COPY yarn.lock ./

# CI and release builds should use npm ci to fully respect the lockfile.
# Local development may use npm install for opportunistic package updates.
RUN yarn
RUN npm rebuild node-sass
ENV NODE_ENV=production

COPY . .
RUN yarn build
CMD ["next", "start"]

# # Build
# FROM test-target as build-target

# # Use build tools, installed as development packages, to produce a release build.

# # Reduce installed packages to production-only.
# # RUN npm prune --production

# # Archive
# FROM node:alpine as archive-target
# ENV NODE_ENV=production
# ENV PATH $PATH:/usr/src/app/node_modules/.bin

# WORKDIR /usr/src/app

# # Include only the release build and production packages.
# COPY --from=build-target /usr/src/app .
