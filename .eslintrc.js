// https://github.com/typescript-eslint/typescript-eslint#getting-started--installation
module.exports = {
	root: true,
	ignorePatterns: [
		".eslintrc.js",
		"/typings/globals.d.ts",
		"tailwind.config.js",
		"postcss.config.js",
		"next.config.js",
		"next-env.d.ts",
	],
	parser: "@typescript-eslint/parser",
	rules: {
		"@typescript-eslint/no-unused-vars": "off",
		"react/react-in-jsx-scope": "off",
		"@typescript-eslint/no-empty-interface": "off",
	},
	plugins: ["@typescript-eslint"],
	settings: {
		react: {
			pragma: "React",
			version: "latest",
		},
	},

	extends: [
		"eslint:recommended",
		"plugin:react/recommended",
		"plugin:@typescript-eslint/recommended",
	],
};
